<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Session;
use App\Certification;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SessionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        return view('newSession', ['id' => $id]);
    }

    public function indexSession($id, Request $request)
    {
        if ($request->ajax()) {
            $data = Session::where('id_certif', $id)->orderBy('date', 'asc')->get();
              return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn(
                'action', function ($row) {

                      $btn = '<a href="'.route('sessions.delete', ['ids' => $row->id]).'" class="edit btn btn-danger btn-sm">Supprimer</a>';

                        return $btn;
                }
            )
              ->rawColumns(['action'])
              ->make(true);
    }

        return view('session', ['id' => $id]);
    }

    public function createSession($id, Request $request ) {



        $id_certif = $id;
        $date = $request->get('date');
        $type = $request->get('type');
        $formateur = $request->get('formateur');

        $d = date('Y-m-d H:i:s', strtotime($date));

        $session = Session::create(
            [
                'id_certif' => $id_certif,
                'date' => $d,
                'type' => $type,
                'formateur' => $formateur
            ]
        );

        if ($session) {
            $certification = Certification::find($id_certif);

            return back();
        } else {
            return view('errors.401');
        }

    }

    public function deleteSession($ids)
    {

        $session = Session::destroy($ids);

        if ($session) {
            return back();
        } else {
            return view('errors.401');
        }
    }

    public function planning(Request $request)
    {
        $user = Auth::user();
        $id_user = $user->id;
        $created_at =  DB::table('user_certif')->where('id_user', $id_user)->max('created_at');

        $id_certif =  DB::table('user_certif')->where('created_at', $created_at)->value('id_certif');


        if ($request->ajax()) {
            $data = Session::where('id_certif', $id_certif)->orderBy('date', 'asc')->get();
              return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn(
                'nom', function ($row) {

                    $certification = Certification::find($row->id);
                    $nom_certif = $certification->nom_certif;

                      $label = '<label for="email-form1-24" class="form-control-label mbr-fonts-style display-7">'.$nom_certif.'</label>';

                        return $label;
                }
            )
              ->rawColumns(['nom'])
              ->make(true);
    }

        return view('planning');

    }
}
