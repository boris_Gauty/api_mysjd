<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Certification;
use App\Resultat;
use App\Notification;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use DateTime;
use Illuminate\Foundation\Auth\User;

class CertificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Certification::latest()->get();
              return Datatables::of($data)
                  ->addIndexColumn()
                ->addColumn(
                    'action', function ($row) {

                        $btn = '<a href="'.route('certif', ['id' => $row->id]).'" class="edit btn btn-primary btn-sm">Selectionner</a>';

                        return $btn;
                    }
                )
            ->setRowClass(
                function ($row) {
                    if($row->nom_certif == "Word 2016" || $row->nom_certif == "Word 2013") {
                        return 'alert-primary';
                    } else if ($row->nom_certif == "Excel 2016" || $row->nom_certif == "Excel 2013") {
                        return 'alert-success';
                    } else if ($row->nom_certif == "Powerpoint 2016" || $row->nom_certif == "Powerpoint 2013") {
                        return 'alert-warning';
                    }
                }
            )
                 ->rawColumns(['action'])
                 ->make(true);
        }

        return view('certification');
    }

    public function certif($id)
    {
        $certification = Certification::find($id);

        return view('certif', ['id' => $id,'title' => $certification->nom_certif]);
    }

    public function newCertif()
    {

        return view('newcertif');
    }


    public function createCertif(Request $request)
    {

        $date_debut = $request->get('date_debut');
        $date_fin = $request->get('date_fin');

        $dd = date('Y-m-d H:i:s', strtotime($date_debut));
        $df = date('Y-m-d H:i:s', strtotime($date_fin));

        $certification = Certification::create(
            [
                'nom_certif' => $request->get('nom_certif'),
                'date_debut' => $dd,
                'date_fin' => $df,
                'niveau' => $request->get('niveau')
            ]
        );

        if ($certification) {
            return view('home1');
        } else {
            return view('errors.401');
        }
    }

    public function deleteCertif(Request $request)
    {

        $id = $request->get('id');

        $certification = Certification::destroy($id);

        if ($certification) {
            return view('home1');
        } else {
            return view('errors.401');
        }
    }

    public function inscription($id, Request $request)
    {
        $user = Auth::user();
        $certification = Certification::find($id);
        $date_fin = $certification->date_fin;

        $dteStart = new DateTime($date_fin);
        $dteEnd   = new DateTime();

        $dteDiff  = $dteEnd->diff($dteStart);

        $d = $dteDiff->format("%d");

        if ($d >= 10) {
            DB::table('user_certif')->insert(
                ['id_user' => $user->id, 'id_certif' => $id,'created_at' => $dteEnd]
            );

            return back()->withInput();
        }

        else {
            return view('errors.429');
        }

    }

    public function desinscription($id)
    {
        $user = Auth::user();
        $id_certif_user = DB::table('user_certif')->where([['id_user', $user->id],['id_certif', $id]])->value('id');

        DB::table('user_certif')->where('id', $id_certif_user)->delete();

        return back()->withInput();
    }

    public function showCertif(Request $request)
    {
        if ($request->ajax()) {
            $datetime = date("Y-m-d H:i:s");
            $user = Auth::user();
            $niveau = $user->niveau;

            $data = Certification::where([['date_fin', '>=', $datetime],['niveau', $niveau]])->orderBy('date_fin', 'asc')->get();
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn(
                        'action', function ($row) {

                            $user = Auth::user();
                            $id_certif_user = DB::table('user_certif')->where([['id_user', $user->id],['id_certif', $row->id]])->first();

                            if($id_certif_user == null) {

                                $btn = '<a href="'.route('inscription', ['id' => $row->id]).'" class="edit btn btn-primary btn-sm">S\'inscrire</a>';

                                return $btn;
                            } else {
                                $btn = '<a href="'.route('desinscription', ['id' => $row->id]).'" class="edit btn btn-danger btn-sm">Se désinscrire</a>';

                                return $btn;
                            }

                        }
                    )
            ->setRowClass(
                function ($row) {
                    if($row->nom_certif == "Word 2016" || $row->nom_certif == "Word 2013") {
                        return 'alert-primary';
                    } else if ($row->nom_certif == "Excel 2016" || $row->nom_certif == "Excel 2013") {
                        return 'alert-success';
                    } else if ($row->nom_certif == "Powerpoint 2016" || $row->nom_certif == "Powerpoint 2013") {
                        return 'alert-warning';
                    }
                }
            )
              ->rawColumns(['action'])
              ->make(true);
        }

        return view('showCertif');
    }

    public function listInscrit($id, Request $request)
    {
        $user =  DB::table('user_certif')->where('id_certif', $id)->get();

        if ($request->ajax()) {
            foreach ($user as $users)
            {
                  //  $data = DB::table('users')->where('id', $users->id_user)->get();
                    $data = User::where('id', $users->id_user)->get();
                      return Datatables::of($data)
                          ->addIndexColumn()
                          ->make(true);


            }


        }

        return view('listuser', ['id' => $id]);

    }

    public function getUser($id, Request $request)
    {
        $user =  DB::table('user_certif')->where('id_certif', $id)->get();
        $certification = Certification::find($id);




        foreach ($user as $users)
            {
              //$data = DB::table('users')->where('id', $users->id_user)->get();
                $data = User::where('id', $users->id_user)->get();

        }

            return view('result', compact('data', 'certification'));
    }

    public function updateResult($id, Request $request)
    {
            $id_user = $request->get('id_user');
            $points = $request->get('point');


            $id_user_certif =  DB::table('user_certif')->where([['id_certif', $id],['id_user', $id_user]])->value('id');

            $result = Resultat::where('id_user_certif', $id_user_certif)->first();

            $r = (string) $result;

        if ($r == null) {
            $resultat = Resultat::create(['id_user_certif' => $id_user_certif, 'points' => $points]);

            if ($resultat) {
                return back();
            } else {
                return view('errors.429');
            }
        } else {
            $re = Resultat::find($result->id);
            $re->id_user_certif = $id_user_certif;
            $re->points = $points;
            $re->save();

            if ($re) {
                return back();
            } else {
                return view('errors.429');
            }
        }




    }

    public function getResult(Request $request)
    {
        $user = Auth::user();
        $id_user = $user->id;

        $user_certif = DB::table('user_certif')->where('id_user', $id_user)->get();

        if ($request->ajax()) {
            foreach ($user_certif as $certification) {
                $data = Certification::where('id', $certification->id_certif)->get();
                  return Datatables::of($data)
                      ->addIndexColumn()
                      ->addColumn(
                        'points', function ($row) {

                            $id_user_certif = DB::table('user_certif')->where('id_certif', $row->id)->value('id');
                            $result = Resultat::where('id_user_certif', $id_user_certif)->first();
                            $point = $result->points;

                            $label = '<label for="email-form1-24" class="form-control-label mbr-fonts-style display-7">'.$point.'</label>';

                            return $label;
                        }
                    )
                    ->setRowClass(
                        function ($row) {
                            if($row->nom_certif == "Word 2016" || $row->nom_certif == "Word 2013") {
                                return 'alert-primary';
                            } else if ($row->nom_certif == "Excel 2016" || $row->nom_certif == "Excel 2013") {
                                return 'alert-success';
                            } else if ($row->nom_certif == "Powerpoint 2016" || $row->nom_certif == "Powerpoint 2013") {
                                return 'alert-warning';
                            }
                        }
                    )
                  ->rawColumns(['points'])
                  ->make(true);
            }
        }

        return view('resultat');
    }

    public function newNotif($id, Request $request)
    {
        $valeur = $request->get('valeur');
        $type ="normale";

        $notification = Notification::create(['id_certif' => $id, 'type' => $type, 'valeur' => $valeur]);

        if ($notification) {
            $certification = Certification::find($id);

            return view('certif', ['id' => $id,'title' => $certification->nom_certif]);
        } else {
            return view('errors.429');
        }
    }

    public function notif($id)
    {

        return view('newNotif', ['id' => $id]);
    }

    public function showNotif(Request $request)
    {
        $user = Auth::user();
        $id_user = $user->id;

        $user_certif = DB::table('user_certif')->where('id_user', $id_user)->get();


        if ($request->ajax()) {
            foreach ($user_certif as $certifications) {
                $data = Notification::where('id_certif', $certifications->id_certif)->orderBy('created_at', 'desc')->get();
                  return Datatables::of($data)
                      ->addIndexColumn()
                      ->addColumn(
                        'nom', function ($row) {

                            $certification = Certification::where('id', $row->id_certif)->first();
                            $nom = $certification->nom_certif;

                            $label = '<label for="email-form1-24" class="form-control-label mbr-fonts-style display-7">'.$nom.'</label>';

                            return $label;
                        }
                    )
                  ->rawColumns(['nom'])
                  ->make(true);
            }
        }

        return view('showNotif');
    }



}
