<!DOCTYPE html>
<html>

<head>
    <!-- Site made with Mobirise Website Builder v4.9.7, https://mobirise.com -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="Mobirise v4.9.7, mobirise.com">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/logosjd-99x109-1.png" type="image/x-icon">
    <meta name="description" content="Session des candidats">

    <title>Etudiant</title>
    <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
    <link rel="stylesheet" href="assets/tether/tether.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="assets/datatables/data-tables.bootstrap4.min.css">
    <link rel="stylesheet" href="assets/dropdown/css/style.css">
    <link rel="stylesheet" href="assets/socicon/css/styles.css">
    <link rel="stylesheet" href="assets/theme/css/style.css">
    <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">



</head>

<body>
    <section class="menu cid-rpl1P51lp2" once="menu" id="menu1-q">



        <nav
            class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <div class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </button>
            <div class="menu-logo">
                <div class="navbar-brand">
                    <span class="navbar-logo">
                        <a href="https://www.univ-catho-sjd.cm">
                            <img src="assets/images/logosjd-bleufonc-67x94.png" alt="Mobirise" title=""
                                style="height: 3.8rem;">
                        </a>
                    </span>
                    <span class="navbar-caption-wrap"><a class="navbar-caption text-white display-4"
                            href="{{ route('home') }}">CertiSJD</a></span>
                </div>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
                    <li class="nav-item">
                        <a class="nav-link link text-warning display-4" href="https://mobirise.co">
                            <font face="MobiriseIcons"><br></font> &nbsp; &nbsp;
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link link text-warning display-4">{{ Auth::user()->name }}</a>
                    </li>
                    <li class="nav-item"><a class="nav-link link text-warning display-4"
                            href="{{ route('home') }}"><span
                                class="mbri-home mbr-iconfont mbr-iconfont-btn"></span></a></li>

                    <li class="nav-item"><a class="nav-link link text-warning display-4" href="faq.html">
                            FAQ</a></li>

                </ul>
                <div class="navbar-buttons mbr-section-btn"><a class="btn btn-sm btn-primary display-4"
                        href="{{ route('logout') }}">Déconnexion</a></div>
            </div>
        </nav>
    </section>

    <section class="engine"><a href="https://mobirise.info/d">site maker</a></section>
    <section class="features1 cid-rpiTJckeuw" id="features1-9">




        <div class="container">
            <div class="media-container-row">

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-img pb-3">
                        <a href="{{ route('show.certifications.index') }}"><span class="mbr-iconfont mbri-contact-form"
                                style="color: rgb(15, 118, 153); fill: rgb(15, 118, 153);"></span></a>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-5">
                            S'inscrire à une certification</h4>
                        <p class="mbr-text mbr-fonts-style display-7"></p>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-img pb-3">
                        <a href="{{ route('show.notif') }}"><span class="mbr-iconfont mbri-mobile"
                                style="color: rgb(15, 118, 153); fill: rgb(15, 118, 153);"></span></a>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-5">
                            Mes Notifications</h4>
                        <p class="mbr-text mbr-fonts-style display-7"></p>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-img pb-3">
                        <a href="{{ route('planning.index') }}"><span class="mbr-iconfont mbri-calendar"
                                style="color: rgb(15, 118, 153); fill: rgb(15, 118, 153);"></span></a>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-5">
                            Planning des Certifications</h4>
                        <p class="mbr-text mbr-fonts-style display-7"></p>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-img pb-3">
                        <a href="{{ route('resultat.show') }}">  <span class="mbr-iconfont mbri-success"
                            style="color: rgb(15, 118, 153); fill: rgb(15, 118, 153);"></span></a>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-5">
                            Résultats</h4>
                        <p class="mbr-text mbr-fonts-style display-7"></p>
                    </div>
                </div>

            </div>

        </div>

    </section>



    <section class="footer3 cid-rpiLcyKbpw mbr-reveal" id="footer3-6">





        <div class="container">
            <div class="media-container-row align-center mbr-white">

                <div class="row social-row">
                    <div class="social-list align-right pb-2">






                        <div class="soc-item">
                            <a href="https://twitter.com/AE_SJD" target="_blank">
                                <span class="mbr-iconfont mbr-iconfont-social socicon-twitter socicon"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://m.facebook.com/AEStJerome/" target="_blank">
                                <span class="mbr-iconfont mbr-iconfont-social socicon-facebook socicon"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://www.instagram.com/aesjd/" target="_blank">
                                <span class="mbr-iconfont mbr-iconfont-social socicon-instagram socicon"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row row-copirayt">
                    <p class="mbr-text mb-0 mbr-fonts-style mbr-white align-center display-7">
                        © Copyright 2019 IUCSJD - All Rights Reserved<br>Designed by <a
                            href="mailto:vboutchom@univ-catho-sjd.com" class="text-white">BMCV</a>&nbsp;&amp; <a
                            href="mailto:jjidjou@univ-catho-sjd.com" class="text-white">JJD</a></p>
                </div>
            </div>
        </div>
    </section>


    <script src="assets/web/assets/jquery/jquery.min.js"></script>
    <script src="assets/popper/popper.min.js"></script>
    <script src="assets/tether/tether.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/smoothscroll/smooth-scroll.js"></script>
    <script src="assets/datatables/jquery.data-tables.min.js"></script>
    <script src="assets/datatables/data-tables.bootstrap4.min.js"></script>
    <script src="assets/dropdown/js/script.min.js"></script>
    <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
    <script src="assets/theme/js/script.js"></script>
    <script src="assets/formoid/formoid.min.js"></script>


</body>

</html>
