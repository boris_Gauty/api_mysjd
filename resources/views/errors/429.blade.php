@extends('errors.illustrated-layout')

@section('title', __('Erreur de connexion'))
@section('code', '#')
@section('message', __('Verifiez vos paramètres de connexion'))
