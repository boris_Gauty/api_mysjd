@extends('errors.illustrated-layout')

@section('title', __('Aucun inscrit à cette vague'))
@section('code', '#')
@section('message', __('Aucun inscrit à cette vague'))
