<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- Site made with Mobirise Website Builder v4.9.7, https://mobirise.com -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="Mobirise v4.9.7, mobirise.com">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/logosjd-99x109-1.png" type="image/x-icon">
    <meta name="description" content="Page d'inscription et de connexion sur la plateforme">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Accueil</title>
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/testcss.css">
    <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
    <link rel="stylesheet" href="assets/tether/tether.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="assets/dropdown/css/style.css">
    <link rel="stylesheet" href="assets/socicon/css/styles.css">
    <link rel="stylesheet" href="assets/theme/css/style.css">
    <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">



</head>

<body>
    <section class="menu cid-rpl1I9gAmb" once="menu" id="menu1-p">



        <nav id="navi"
            class=" navbar1 navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
            <!--<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <div class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </button>-->
            <div class="menu-logo">
                <div class="navbar-brand" id="log1">
                    <span class="navbar-logo">
                        <a href="https://www.univ-catho-sjd.cm">
                            <img src="assets/images/logosjd-bleufonc-67x94.png" alt="SJD" title=""
                                style="height: 2.8rem;">
                        </a>
                    </span>
                    <span class="navbar-caption-wrap"><a class="navbar-caption text-white display-6"
                            href="{{ route('home') }}">CertiSJD</a></span>
                </div>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <form action="{{ route('login') }}" method="POST" class="form-inline" id="form11">
                        @csrf
                    <div class="divform11">
                        <input type="text" name="username" id="username" placeholder="Votre nom d'utilisateur" required="required"
                            autofocus>

                            @error('username')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="divform11">
                        <input type="password" name="password" placeholder="password" required="required">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                    <div class="input-group-btn  mt-2 divform11" id="boutin">
                        <button type="submit" class="btn btn-primary btn-form">Connexion</button>
                    </div>
                </form>
                <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
                    <li class="nav-item">
                        <a class="nav-link link text-white" href="https://mobirise.co">
                            <font face="MobiriseIcons"><br></font> &nbsp; &nbsp;
                        </a>
                    </li>
                    <li class="nav-item">
                        <!--<form class="form-inline">
                                        <div class="form-group mb-2">
                                          <label for="staticEmail2" class="sr-only">Email</label>
                                          <input type="text" readonly class="form-control-plaintext" id="staticEmail2" value="email@example.com">
                                        </div>
                                        <div class="form-group mx-sm-3 mb-2">
                                          <label for="inputPassword2" class="sr-only">Password</label>
                                          <input type="password" class="form-control" id="inputPassword2" placeholder="Password">
                                        </div>
                                        <button type="submit" class="btn btn-primary mb-2">Confirm identity</button>
                                    </form>-->


                        <!--<input type="text" name="login" placeholder="login" data-form-field="login" required="required" class="form-control input display-7" id="login-form4-1">-->

                    </li>
                </ul>
            </div>
        </nav>
    </section>

    <section class="engine"><a href="https://mobirise.info/l">free site templates</a></section>
    <section class="mbr-section form4 cid-roMpyW28t7 mbr-parallax-background" id="form4-1">



        <div class="carousel2 mbr-overlay" style="opacity: 0.6; background-color: rgb(0, 0, 0);">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6" id="carousel11">
                    <h2 class="pb-3 align-left mbr-fonts-style display-5" id="bienvenue"><em>
                            Bienvenue sur la plateforme certiSJD</em></h2>
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators" id="carousel-indicator">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active"><label for="item11">Vous pouvez vous connecter directement
                                    si vous avez un compte</label>
                                <!--<img src="..." class="d-block w-100" alt="...">-->
                            </div>
                            <div class="carousel-item"><label for="item11">Inscrivez-vous et profitez des privilèges que
                                    nous vous offrons</label>
                                <!--<img src="..." class="d-block w-100" alt="...">-->
                            </div>
                            <div class="carousel-item"><label for="item11">Enregidtrez-vous pour participer à une
                                    formation ou un examen en fonction des critères d'elligibilité dont nous vous avons
                                    fait part et obtenez une certification valide deux années renouvelables</label>
                                <!--<img src="..." class="d-block w-100" alt="...">-->
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                            data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                            data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <!--<div class="google-map"><iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15919.480870445965!2d9.6954035!3d4.04689075!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x106112efe7ab6d55%3A0x637a92627a1e40b8!2sUniversit%C3%A9+Catholique+Saint+J%C3%A9r%C3%B4me+De+Douala!5e0!3m2!1sfr!2scm!4v1556786517332!5m2!1sfr!2scm" allowfullscreen=""></iframe></div>-->
                </div>

                </div>
            </div>
        </div>
    </section>

    <section once="footers" class="cid-roRiiuleHN" id="footer7-2">





        <div class="container">
            <div class="media-container-row align-center mbr-white">

                <div class="row social-row">
                    <div class="social-list align-right pb-2">






                        <div class="soc-item">
                            <a href="https://twitter.com/AE_SJD" target="_blank">
                                <span class="mbr-iconfont mbr-iconfont-social socicon-twitter socicon"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://m.facebook.com/AEStJerome/" target="_blank">
                                <span class="mbr-iconfont mbr-iconfont-social socicon-facebook socicon"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://www.instagram.com/aesjd/" target="_blank">
                                <span class="mbr-iconfont mbr-iconfont-social socicon-instagram socicon"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row row-copirayt">
                    <p class="mbr-text mb-0 mbr-fonts-style mbr-white align-center display-7">
                        © Copyright 2019 IUCSJD - All Rights Reserved<br>Designed by <a
                            href="mailto:vboutchom@univ-catho-sjd.com" class="text-white">BMCV</a>&nbsp;&amp; <a
                            href="mailto:jjidjou@univ-catho-sjd.com" class="text-white">JJD</a></p>
                </div>
            </div>
        </div>
    </section>


    <script src="assets/web/assets/jquery/jquery.min.js"></script>
    <script src="assets/popper/popper.min.js"></script>
    <script src="assets/tether/tether.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/smoothscroll/smooth-scroll.js"></script>
    <script src="assets/dropdown/js/script.min.js"></script>
    <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
    <script src="assets/parallax/jarallax.min.js"></script>
    <script src="assets/theme/js/script.js"></script>
    <script src="assets/formoid/formoid.min.js"></script>



    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#champ_cache1').hide(); // on cache les champs par défaut
            $('#champ_cache2').hide();
            $('#champ_cache3').hide();

            $('select[id="cacher"]').change(function () { // lorsqu'on change de valeur dans la liste
                var valeur = $(this).val(); // valeur sélectionnée

                if (valeur != '') { // si non vide
                    if (valeur == 'Etudiant') { // si "Etudiant"
                        $('#champ_cache1').show();
                        $('#champ_cache2').show();
                        $('#champ_cache3').hide();
                    } else {
                        $('#champ_cache1').hide();
                        $('#champ_cache2').hide();
                        $('#champ_cache3').show();
                    }
                }
            });
        });


        /*$('select[id="aaa"]').change(function() { // lorsqu'on change de valeur dans la liste
        var valeur = $(this).val(); // valeur sélectionnée

        if(valeur != '') { // si non vide
            if(valeur == 'Externe') { // si "Etudiant"
            $('#champ_cache3').show();
            }
            else {
            $('#champ_cache1').hide();
            $('#champ_cache2').hide();
            $('#champ_cache3').hide();
            }
        }
        });*/

        // });//


        /*document.getElementById("champ_cache1").style.display = "none";
        document.getElementById("champ_cache2").style.display = "none";
        document.getElementById("champ_cache3").style.display = "none";
        function afficher1(cacher)
        {
          document.getElementById("champ_cache1").style.display = "block";
          document.getElementById("champ_cache2").style.display = "block";
          document.getElementById("champ_cache3").style.display = "none";
        }
        function afficher2()
        {
           document.getElementById("champ_cache3").style.display = "block";
           document.getElementById("champ_cache1").style.display = "none";
        document.getElementById("champ_cache2").style.display = "none";
        }*/

        /*function cacher()
        {
           document.getElementById("champ_cache").style.display = "none";
        }*/



        /*document.getElementById("champ_cache1").style.display = "none";
        document.getElementById("champ_cache2").style.display = "none";
        document.getElementById("champ_cache3").style.display = "none";

        document.getElementById("cacher").onchange(function afficher1()
        {

          if(document.getElementById("cacher").value == "Etudiant") {
          document.getElementById("champ_cache1").style.display = "block";
          document.getElementById("champ_cache2").style.display = "block";
          document.getElementById("champ_cache3").style.display = "none";
        }
        else if(document.getElementById("cacher").value == "Externe") {
          document.getElementById("champ_cache1").style.display = "none";
          document.getElementById("champ_cache2").style.display = "none";
          document.getElementById("champ_cache3").style.display = "block";
        }
        });
        function afficher2()
        {
           document.getElementById("champ_cache3").style.display = "block";
        }*/
        /*function cacher()
        {
           document.getElementById("champ_cache").style.display = "none";
        }*/

    </script>


</body>

</html>
