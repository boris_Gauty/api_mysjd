<!DOCTYPE html>
<html  >
<head>
  <!-- Site made with Mobirise Website Builder v4.9.7, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.9.7, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="{{ asset('assets/images/logosjd-99x109-1.png') }}" type="image/x-icon">
  <meta name="description" content="Formulaire d'inscription aux formations">

  <title>Nouvelle Session</title>
  <link rel="stylesheet" href="{{ asset('assets/web/assets/mobirise-icons/mobirise-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/tether/tether.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap-grid.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap-reboot.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/datatables/data-tables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/dropdown/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/socicon/css/styles.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/theme/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/mobirise/css/mbr-additional.css') }}" type="text/css">
  <meta name="csrf-token" content="{{ csrf_token() }}">



</head>
<body>
        <section class="menu cid-rpl1P51lp2" once="menu" id="menu1-q">



                <nav
                    class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <div class="hamburger">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </button>
                    <div class="menu-logo">
                        <div class="navbar-brand">
                            <span class="navbar-logo">
                                <a href="https://www.univ-catho-sjd.cm">
                                    <img src="{{ asset('assets/images/logosjd-bleufonc-67x94.png') }}" alt="Mobirise" title=""
                                        style="height: 3.8rem;">
                                </a>
                            </span>
                            <span class="navbar-caption-wrap"><a class="navbar-caption text-white display-4"
                                    href="{{ route('home1') }}">CertiSJD</a></span>
                        </div>
                    </div>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
                            <li class="nav-item">
                                <a class="nav-link link text-warning display-4" href="https://mobirise.co">
                                    <font face="MobiriseIcons"><br></font> &nbsp; &nbsp;
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link link text-warning display-4">{{ Auth::user()->name }}</a>
                            </li>
                            <li class="nav-item"><a class="nav-link link text-warning display-4"
                                    href="{{ route('home1') }}"><span
                                        class="mbri-home mbr-iconfont mbr-iconfont-btn"></span></a></li>

                            <li class="nav-item"><a class="nav-link link text-warning display-4" href="faq.html">
                                    FAQ</a></li>

                        </ul>
                        <div class="navbar-buttons mbr-section-btn"><a class="btn btn-sm btn-primary display-4"
                                href="{{ route('logout') }}">Déconnexion</a></div>
                    </div>
                </nav>
            </section>

<section class="engine"><a href="https://mobirise.info/p">web templates free download</a></section><section class="mbr-section form1 cid-rpvEUmzgmb" id="form1-24">




    <div class="container">
        <div class="row justify-content-center">
            <div class="title col-12 col-lg-8">
                <h2 class="mbr-section-title align-center pb-3 mbr-fonts-style display-2">
                    Creer une nouvelle Session de certification</h2>
                <h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-5">
                    Remplissez les champs vides de ce formulaire.</h3>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="media-container-column col-lg-8">

                <form action="{{ route('new.session', ['id' => $id]) }}" method="POST" class="mbr-form form-with-styler" data-form-title="Mobirise Form">
                    @csrf
                    <div class="dragArea row row-sm-offset">

                        <div class="col-md-4  form-group" data-for="email">
                            <label for="email-form1-24" class="form-control-label mbr-fonts-style display-7">Jour de la Session</label>
                            <input type="date" name="date"  data-form-field="Date" required="required" class="form-control display-7" id="email-form1-24">
                        </div>
                        <div class="col-md-4  form-group" data-for="email">
                                <label for="email-form1-24" class="form-control-label mbr-fonts-style display-7">Nom du Formateur</label>
                                <input type="text" name="formateur"  data-form-field="Name" required="required" class="form-control display-7" id="email-form1-24">
                        </div>
                       <div class="col-md-4  form-group" data-for="name">
                                <label for="name-form1-24" class="form-control-label mbr-fonts-style display-7">Type de Session</label>

                                <select name="type" type="text"  data-form-field="Name" required="required" class="form-control display-7" id="name-form1-24">
                                        <option value="Normale">Normale</option>
                                        <option value="Rattrapage">Rattrapage</option>
                                      </select>
                            </div>
                        <div class="col-md-12 input-group-btn align-center">
                            <button type="submit" class="btn btn-primary btn-form display-4">Valider</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<section once="footers" class="cid-rpvKQtjTce" id="footer7-25">





    <div class="container">
        <div class="media-container-row align-center mbr-white">

            <div class="row social-row">
                <div class="social-list align-right pb-2">






                <div class="soc-item">
                        <a href="https://twitter.com/AE_SJD" target="_blank">
                            <span class="mbr-iconfont mbr-iconfont-social socicon-twitter socicon"></span>
                        </a>
                    </div><div class="soc-item">
                        <a href="https://m.facebook.com/AEStJerome/" target="_blank">
                            <span class="mbr-iconfont mbr-iconfont-social socicon-facebook socicon"></span>
                        </a>
                    </div><div class="soc-item">
                        <a href="https://www.instagram.com/aesjd/" target="_blank">
                            <span class="mbr-iconfont mbr-iconfont-social socicon-instagram socicon"></span>
                        </a>
                    </div></div>
            </div>
            <div class="row row-copirayt">
                <p class="mbr-text mb-0 mbr-fonts-style mbr-white align-center display-7">
                    © Copyright 2019 IUCSJD - All Rights Reserved<br>Designed by <a href="mailto:vboutchom@univ-catho-sjd.com" class="text-white">BMCV</a>&nbsp;&amp; <a href="mailto:jjidjou@univ-catho-sjd.com" class="text-white">JJD</a></p>
            </div>
        </div>
    </div>
</section>


<script src="{{ asset('assets/web/assets/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/popper/popper.min.js') }}"></script>
<script src=" {{ asset('assets/tether/tether.min.js') }}"></script>
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/smoothscroll/smooth-scroll.js') }}"></script>
<script src="{{ asset('assets/datatables/jquery.data-tables.min.js') }}"></script>
<script src="{{ asset('assets/datatables/data-tables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/dropdown/js/script.min.js') }}"></script>
<script src="{{ asset('assets/touchswipe/jquery.touch-swipe.min.js') }}"></script>
<script src=" {{ asset('assets/theme/js/script.js') }}"></script>
<script src="{{ asset('assets/formoid/formoid.min.js') }}"></script>


</body>
</html>
