<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(
    '/', function () {
        return view('welcome');
    }
)->name('/');

 //Auth::routes();

 Route::post('login', 'Auth\LoginController@attemptLogin')->name('login');
 Route::get('logout', 'Auth\LoginController@logout')->name('logout');

 Route::get('/home', 'HomeController@index')->name('home');

 Route::get('/home1', 'HomeController@index1')->name('home1');

 //Certifications

 Route::get('/nouvelle certification', 'CertificationController@newCertif')->name('newCertif');

 Route::post('/nouvelle certification', 'CertificationController@createCertif')->name('new.certif');

 Route::get('/nouvelle notification/{id}', 'CertificationController@notif')->name('newNotif');

 Route::post('/nouvelle notification/{id}', 'CertificationController@newNotif')->name('new.notif');

 Route::get('/certifications', ['uses'=>'CertificationController@index', 'as'=>'certifications.index']);

 Route::get('/certification', ['uses'=>'CertificationController@showCertif', 'as'=>'show.certifications.index']);

 Route::get('/notifs', ['uses'=>'CertificationController@showNotif', 'as'=>'show.notif']);

 Route::get('/certif/{id}', ['uses'=>'CertificationController@certif', 'as'=>'certif']);

 Route::get('/inscription/{id}', ['uses'=>'CertificationController@inscription', 'as'=>'inscription']);

 Route::get('/desinscription/{id}', ['uses'=>'CertificationController@desinscription', 'as'=>'desinscription']);

 Route::get('/listuser/{id}', ['uses'=>'CertificationController@listInscrit', 'as'=>'list.index']);

 Route::get('/result/{id}', ['uses'=>'CertificationController@getUser', 'as'=>'result']);

 Route::get('/resultat', ['uses'=>'CertificationController@getResult', 'as'=>'resultat.show']);

 Route::post('/updateresult/{id}', ['uses'=>'CertificationController@updateResult', 'as'=>'result.edit']);

 //Sessions

 Route::get('/nouvelle Session/{id}', 'SessionController@index')->name('newSession');

 Route::post('/nouvelle Session/{id}', 'SessionController@createSession')->name('new.session');

 // Route::get('/certifications/index/{id}', 'CertificationController@indexCertif')->name('certif.index');

 Route::get('/sessions/{id}', ['uses'=>'SessionController@indexSession', 'as'=>'sessions.index']);

 Route::get('/planning', ['uses'=>'SessionController@planning', 'as'=>'planning.index']);

 Route::get('/sessionsdelete/{ids}', ['uses'=>'SessionController@deleteSession', 'as'=>'sessions.delete']);





